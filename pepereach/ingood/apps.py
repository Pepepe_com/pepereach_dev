from django.apps import AppConfig


class IngoodConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ingood'
